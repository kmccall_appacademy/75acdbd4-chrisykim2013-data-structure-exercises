# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  arr.max - arr.min
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  arr == arr.sort
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  vowels = ["a", "e", "i", "o", "u"]
  count = 0
  vowels.each {|el| count += str.downcase.count(el)}
  count
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  # str.delete("aeiouAEIOU") works but let's try with arrays
  vowels = ["a", "e", "i", "o", "u"]
  new_arr = str.chars
  vowels.each {|el| new_arr.delete(el)}
  vowels.each {|el| new_arr.delete(el.upcase)}
  new_arr.join
end


# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  int.to_s.chars.sort.reverse
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  new_arr = str.downcase.chars
  new_arr!= new_arr.uniq
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  "(#{arr[0]}#{arr[1]}#{arr[2]}) #{arr[3]}#{arr[4]}#{arr[5]}-#{arr[6]}#{arr[7]}#{arr[8]}#{arr[9]}"
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  new_arr = str.split(",")
  new_arr.max.to_i - new_arr.min.to_i
end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset=1)
  part_one = arr.drop(offset % arr.length)
  part_two = arr.take(offset % arr.length)
  part_one + part_two
end
